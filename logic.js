// N8MZIE1KJ85TGKQZ
function findMax(data,time){
	var dateString="",maximum=0,count=0;
	if(time=="year"){
		for(k in data){
			if(count==365){
				return data[dateString]["2. high"]
			}else{
				if(data[k]["2. high"]>maximum ){
					dateString = k;
					maximum = data[k]["2. high"]
				}
				count++;
			}
		}
	}else if(time=="month"){
		for(k in data){
			if(count==30){
				return data[dateString]["2. high"]
			}else{
				if(data[k]["2. high"]>maximum ){
					dateString = k;
					maximum = data[k]["2. high"]
				}
				count++;
			}
		}
	}else if(time=="week"){
		for(k in data){
			if(count==5){
				return data[dateString]["2. high"]
			}else{
				if(data[k]["2. high"]>maximum ){
					dateString = k;
					maximum = data[k]["2. high"]
				}
				count++;
			}
		}
	}
}
function findMin(data,time){
	var dateString="",maximum=10000,count=0;
	if(time=="year"){
		for(k in data){
			if(count==365){
				return data[dateString]["3. low"]
			}else{
				if(data[k]["3. low"]<maximum ){
					dateString = k;
					maximum = data[k]["3. low"]
				}
				count++;
			}
		}
	}else if(time=="month"){
		for(k in data){
			if(count==30){
				return data[dateString]["3. low"]
			}else{
				if(data[k]["3. low"]<maximum ){
					dateString = k;
					maximum = data[k]["3. low"]
				}
				count++;
			}
		}
	}else if(time=="week"){
		for(k in data){
			if(count==5){
				return data[dateString]["3. low"]
			}else{
				if(data[k]["3. low"]<maximum ){
					dateString = k;
					maximum = data[k]["3. low"]
				}
				count++;
			}
		}
	}
}
function average(data,time){
	var ave = [0,0,0,0,0,0,0],count=0;
	var final = [0,0,0,0,0,0,0];
	if(time=="month"){
		for(k in data){
			if(count<30){
				let d= new Date(k);
				let dayNumber = d.getDay();
				ave[dayNumber]+=Number(data[k]["2. high"]);
				final[dayNumber]++;
				count++;
			}else{
				break;
			}
		}
	}else if(time=="week"){
		for(k in data){
			if(count<5){
				let d= new Date(k);
				let dayNumber = d.getDay();
				ave[dayNumber]+=Number(data[k]["2. high"]);
				final[dayNumber]++;
				count++;
			}else{
				break;
			}
		}
	}else if(time=="year"){
		for(k in data){
			if(count<365){
				let d= new Date(k);
				let dayNumber = d.getDay();
				ave[dayNumber]+=Number(data[k]["2. high"]);
				final[dayNumber]++;
				count++;
			}else{
				break;
			}
		}
	}
	for(var i=0;i<7;i++){
		final[i]=ave[i]/final[i];
	}
	return final;
}
function getData(){
	let x= $('#sname').val();
	let y= $('#day').val();
	$.ajax({
	    url:'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol='+x+'&outputsize=full&apikey=N8MZIE1KJ85TGKQZ',
	    data: "JSON",
	    success: function(response) {
	    	console.log(response);
	    	//validate the respones here
		    let minValue = findMin(response["Time Series (Daily)"],y);
		    $("#min").text("Min: "+minValue);
		    let maxValue = findMax(response["Time Series (Daily)"],y);
		    $("#max").text("Max: "+maxValue);
		    let fin = average(response["Time Series (Daily)"],y);
		    $("#mon").text("Monday: "+fin[1].toFixed(2));
		    $("#tue").text("Tuesday: "+fin[2].toFixed(2));
		    $("#wed").text("Wednesday: "+fin[3].toFixed(2));
		    $("#thr").text("Thrusday: "+fin[4].toFixed(2));
		    $("#fri").text("Friday: "+fin[5].toFixed(2));
	    }
		});
	}
function clearData(){
	$("#min").text("Min: ");
	$("#max").text("Min: ");
	$("#mon").text("");
    $("#tue").text("");
    $("#wed").text("");
    $("#thr").text("");
    $("#fri").text("");
}
$(document).ready(function() {
 $('#sub').click(function() {
      getData();
    });
 $('#reset').click(function() {
      clearData();
    });
});